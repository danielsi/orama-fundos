app.controller('fundosCtrl', function($scope, $http, $filter) {
  $scope.listFundo = [];
  $scope.listFundoMenu = [];
  $scope.Estrategias = {};
  $scope.MacroEstrategias = {};
  $scope.DetalhesFundo = {};
  $scope.urlFundos = "https://s3.amazonaws.com/orama-media/json/fund_detail_full.json?limit=1&offset=0&serializer=fund_detail_full";
  let listaFiltrada = [];
      $http.get($scope.urlFundos).then(function(response) {
         $scope.fundos = response.data;
         //$scope.listaFundos = response.data;
         for(f in $scope.fundos){
           $scope.filtro = {};
           /* Para poder organizar os dados necessários a view, montamos um novo
            objteto para incluí-lo em um nova lista organizada */
           let grupo = {"id":$scope.fundos[f].id,
                        "macro_strategy_name":$scope.fundos[f].specification.fund_macro_strategy.name,
                        "main_strategy_name":$scope.fundos[f].specification.fund_main_strategy.name,
                        "fundo_nome":$scope.fundos[f].simple_name,
                        "tipo_fundo":$scope.fundos[f].specification.fund_type,
                        "classe_fundo":$scope.fundos[f].specification.fund_class,
                        "data_cota":$scope.fundos[f].quota_date,
                        "mes_porcentagem":$scope.fundos[f].profitabilities.month,
                        "ano_porcentagem":$scope.fundos[f].profitabilities.year,
                        "doze_m_porcentagem":$scope.fundos[f].profitabilities.m12,
                        "aplicacao_minima":$scope.fundos[f].operability.minimum_initial_application_amount,
                        "prazo_resgate":$scope.fundos[f].operability.retrieval_quotation_days_str,
                        "cotizacao_aplicacao":$scope.fundos[f].operability.application_quotation_days_str,
                        "cotizacao_resgate": $scope.fundos[f].operability.retrieval_quotation_days_str,
                        "liquidacao_resgate": $scope.fundos[f].operability.retrieval_liquidation_days_str,
                        "taxa_administracao":$scope.fundos[f].fees.administration_fee,
                        "cnpj":$scope.fundos[f].cnpj,
                        "selecionado":false
                      };
                       /* Objetos true or false para definir os status de visualização e seleção */
                        $scope.DetalhesFundo[grupo.id] = false;
                        $scope.Estrategias[grupo.main_strategy_name] = false;
                        $scope.MacroEstrategias[grupo.macro_strategy_name] = false;
                        /* Nova lista de objetos para exibir na tabela */
                        $scope.listFundo.push(grupo);
                        /* Nova lista de objetos para ser exibido no menu laterl */
                        $scope.listFundoMenu.push(grupo);
         }
      });
    /* Alterna a visualização da estratégias principais no menu lateral */
     $scope.toggleEstrategia = function(estrategiaMacro){
       if($scope.MacroEstrategias[estrategiaMacro]){
          $scope.MacroEstrategias[estrategiaMacro] = false;
       }else{
          $scope.MacroEstrategias[estrategiaMacro] = true;

       }
     };
   /* Alterna a visualização dos detalhes do fundo */
     $scope.toggleDetalhesFundo = function(id){
       console.log("Fundo: "+id+" => "+$scope.DetalhesFundo[id]);

       if($scope.DetalhesFundo[id]){
          $scope.DetalhesFundo[id] = false;
       }else{
          $scope.DetalhesFundo[id] = true;

       }
     };
  /* INI-  Filtra por main_strategy | Filtro das estratégias principais | ( es6 ) -- testado no chrome, safari e firefox */
     $scope.filtroEstrategia = function(estrategia){
       if($scope.Estrategias[estrategia]){
             $scope.listFundo = $scope.listFundoMenu;
             listaFiltrada =listaFiltrada.concat( $scope.listFundo.filter((item) => item.main_strategy_name==estrategia));
             if(listaFiltrada.length > 0 ){
               $scope.listFundo = listaFiltrada;
             }else{
               $scope.listFundo = $scope.listFundoMenu;
             }
       }else{
         let listaFiltradaAtual  = $scope.listFundo;
         let listaItensAremover  = listaFiltradaAtual.filter((item) => item.main_strategy_name==estrategia);
         $scope.listFundo        = listaFiltradaAtual.filter((itens) => listaItensAremover.indexOf( itens ) < 0);
          if($scope.listFundo.length <= 0 ){
            $scope.listFundo = $scope.listFundoMenu;
          }
       }
     };
  /* FIM -  Filtrar por main_strategy | Filtro das estratégias principais | ( es6 ) -- testado no chrome, safari e firefox */
});/* Fecha fundosCtrl */

/* Filtros para os campos de porcentagem e moeda( máscara de pontuação Brasil ) */
app.filter('porcentagem', function(){
  return function(number){
    let percent = number * 100;
    percent = percent.toFixed(2);
    return percent;
  };
});

app.filter('moeda', function(){
    return function(valor){
      let novoValor = parseInt(valor.replace(/[\D]+/g,'') );
          novoValor = novoValor+'';
          novoValor = novoValor.replace(/([0-9]{2})$/g, ",$1");
     return ( novoValor.length > 6 )?novoValor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2"):novoValor;
    };
});
